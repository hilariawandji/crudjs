// const mongoose = require ('mongoose');

// const connectDB = async () => {
//     try {
//         mongoose.set("strictQuery", false);
//         mongoose.connect(process.env.MONGO_URI, () => console.log
//         ("Mongo connecté")
//         );
//     } catch (err) {
//         console.log(err);
//         process.exit();
//     }
// };

// module.exports = connectDB;

const mongoose = require('mongoose');

// Fonction de connexion réutilisable
const connectToDB = async () => {
  try {
    await mongoose.connect('mongodb+srv://FromScratch:fs1234@cluster0.ymvwcc4.mongodb.net/test', {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    console.log('Connexion à MongoDB établie');
  } catch (error) {
    console.error('Erreur de connexion à MongoDB :', error);
  }
};

module.exports = connectToDB;
