const express = require("express");
const connectToDB = require("./config/db");
const dotenv = require("dotenv").config();
const port = 5000;

// connexion à la DB
//connectDB();
connectToDB;

const app = express();

// middleware qui permet de traiter les données du request
app.use(express.json());
app.use(express.urlencoded({ extended:false }));

app.use("/post", require("./routes/post.routes"));

// lancer le serveur
app.listen(port, () => console.log("Le serveur a demarré au port " + port));
